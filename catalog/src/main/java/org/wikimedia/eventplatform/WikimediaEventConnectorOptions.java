package org.wikimedia.eventplatform;

import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.configuration.ConfigOptions;

public class WikimediaEventConnectorOptions {

    public static final ConfigOption<String> SINK_SERVER =
            ConfigOptions.key("sink-server")
                    .stringType()
                    .noDefaultValue()
                    .withDescription(
                            "Prefix to add to topic when inserting.");

    private WikimediaEventConnectorOptions() {}
}
