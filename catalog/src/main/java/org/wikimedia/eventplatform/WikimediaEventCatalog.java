package org.wikimedia.eventplatform;

import org.apache.commons.compress.utils.Lists;
import org.apache.flink.streaming.connectors.kafka.table.KafkaDynamicTableFactory;
import org.apache.flink.table.api.Schema;
import org.apache.flink.table.api.TableDescriptor;
import org.apache.flink.table.catalog.*;
import org.apache.flink.table.catalog.exceptions.*;
import org.apache.flink.table.catalog.stats.CatalogColumnStatistics;
import org.apache.flink.table.catalog.stats.CatalogTableStatistics;
import org.apache.flink.table.expressions.Expression;
import org.apache.flink.table.factories.Factory;
import org.apache.flink.table.factories.FactoryUtil;
import org.apache.tomcat.util.buf.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wikimedia.eventutilities.core.event.EventStreamFactory;
import org.wikimedia.eventutilities.flink.table.EventTableDescriptorBuilder;

import java.util.*;

import static org.apache.flink.streaming.connectors.kafka.table.KafkaConnectorOptions.*;

public class WikimediaEventCatalog extends GenericInMemoryCatalog {

    private static final Logger LOG = LoggerFactory.getLogger(WikimediaEventCatalog.class);

    private final Map<String, String> options;

    public WikimediaEventCatalog(String name, Map<String, String> options) {
        super(name, "default_database");

        // wmf defaults
        if(!options.containsKey(PROPS_BOOTSTRAP_SERVERS.key())) {
            options.put(PROPS_BOOTSTRAP_SERVERS.key(), "kafka-jumbo1001.eqiad.wmnet:9092");
        }
        if(!options.containsKey(FactoryUtil.FORMAT.key())) {
            options.put(FactoryUtil.FORMAT.key(), "json");
        }

        // We don't have managed tables since this catalog is just a wrapper for kafka
        options.put(FactoryUtil.CONNECTOR.key(), "kafka");

        this.options = options;
    }

    @Override
    public void open() throws CatalogException {

    }

    @Override
    public void close() throws CatalogException {

    }

    @Override
    public Optional<Factory> getFactory() {
        return Optional.of(new WikimediaEventDynamicTableFactory());
    }

    @Override
    public List<String> listDatabases() throws CatalogException {
        return Collections.singletonList(getDefaultDatabase());
    }

    @Override
    public CatalogDatabase getDatabase(String s) throws DatabaseNotExistException, CatalogException {
        return new CatalogDatabaseImpl(Collections.emptyMap(), null);
    }

    @Override
    public boolean databaseExists(String s) throws CatalogException {
        // There are no databases, but return true to pass sql verification
        return true;
    }

    @Override
    public void createDatabase(String s, CatalogDatabase catalogDatabase, boolean b) throws DatabaseAlreadyExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void dropDatabase(String s, boolean b, boolean b1) throws DatabaseNotExistException, DatabaseNotEmptyException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void alterDatabase(String s, CatalogDatabase catalogDatabase, boolean b) throws DatabaseNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> listTables(String s) throws DatabaseNotExistException, CatalogException {
        // TODO: UX Upgrade: List all topics from event config?
        return null;
    }

    @Override
    public List<String> listViews(String s) throws DatabaseNotExistException, CatalogException {
        return null;
    }

    @Override
    public CatalogBaseTable getTable(ObjectPath objectPath) throws TableNotExistException, CatalogException {
        String stream = objectPath.getObjectName();

        EventTableDescriptorBuilder eventTableDescriptorBuilder = EventTableDescriptorBuilder.from(
            Arrays.asList(
                "https://schema.wikimedia.org/repositories/primary/jsonschema",
                "https://schema.wikimedia.org/repositories/secondary/jsonschema"
            ),
            "https://meta.wikimedia.org/w/api.php"
        );
        TableDescriptor tableDescriptor = eventTableDescriptorBuilder
                .eventStream(stream)
                .connector("datagen") // builder requires a connector to build
                .build();

        if (!tableDescriptor.getSchema().isPresent()) {
            throw new TableNotExistException("Table not found. Does the event schema exist?", objectPath);
        }

        Schema tableSchema = tableDescriptor.getSchema().get();

        // TODO: Move to catalog config
        EventStreamFactory eventStreamFactory = EventStreamFactory.from(
            Arrays.asList(
                "https://schema.wikimedia.org/repositories/primary",
                "https://schema.wikimedia.org/repositories/secondary"
            ),
            "https://meta.wikimedia.org/w/api.php/"
        );

        List<String> topics = eventStreamFactory.createEventStream(stream).topics();

        Map<String, String> props = new HashMap<>(options);
        props.put(TOPIC.key(), StringUtils.join(topics, ';'));

        return CatalogTable.of(tableSchema, null, Lists.newArrayList(), props);
    }

    @Override
    public boolean tableExists(ObjectPath objectPath) throws CatalogException {
        return false;
    }

    @Override
    public void dropTable(ObjectPath objectPath, boolean b) throws TableNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void renameTable(ObjectPath objectPath, String s, boolean b) throws TableNotExistException, TableAlreadyExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void createTable(ObjectPath objectPath, CatalogBaseTable catalogBaseTable, boolean b) throws TableAlreadyExistException, DatabaseNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void alterTable(ObjectPath objectPath, CatalogBaseTable catalogBaseTable, boolean b) throws TableNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<CatalogPartitionSpec> listPartitions(ObjectPath objectPath) throws TableNotExistException, TableNotPartitionedException, CatalogException {
        return null;
    }

    @Override
    public List<CatalogPartitionSpec> listPartitions(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec) throws TableNotExistException, TableNotPartitionedException, PartitionSpecInvalidException, CatalogException {
        return null;
    }

    @Override
    public List<CatalogPartitionSpec> listPartitionsByFilter(ObjectPath objectPath, List<Expression> list) throws TableNotExistException, TableNotPartitionedException, CatalogException {
        return null;
    }

    @Override
    public CatalogPartition getPartition(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec) throws PartitionNotExistException, CatalogException {
        return null;
    }

    @Override
    public boolean partitionExists(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec) throws CatalogException {
        return false;
    }

    @Override
    public void createPartition(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec, CatalogPartition catalogPartition, boolean b) throws TableNotExistException, TableNotPartitionedException, PartitionSpecInvalidException, PartitionAlreadyExistsException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void dropPartition(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec, boolean b) throws PartitionNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void alterPartition(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec, CatalogPartition catalogPartition, boolean b) throws PartitionNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> listFunctions(String s) throws DatabaseNotExistException, CatalogException {
        return super.listFunctions(s);
    }

    @Override
    public CatalogFunction getFunction(ObjectPath objectPath) throws FunctionNotExistException, CatalogException {
        return super.getFunction(objectPath);
    }

    @Override
    public boolean functionExists(ObjectPath objectPath) throws CatalogException {
        return super.functionExists(objectPath);
    }

    @Override
    public void createFunction(ObjectPath objectPath, CatalogFunction catalogFunction, boolean b) throws FunctionAlreadyExistException, DatabaseNotExistException, CatalogException {
        super.createFunction(objectPath, catalogFunction, b);
    }

    @Override
    public void alterFunction(ObjectPath objectPath, CatalogFunction catalogFunction, boolean b) throws FunctionNotExistException, CatalogException {
        super.alterFunction(objectPath, catalogFunction, b);
    }

    @Override
    public void dropFunction(ObjectPath objectPath, boolean b) throws FunctionNotExistException, CatalogException {
        super.dropFunction(objectPath, b);
    }

    @Override
    public CatalogTableStatistics getTableStatistics(ObjectPath objectPath) throws TableNotExistException, CatalogException {
        return null;
    }

    @Override
    public CatalogColumnStatistics getTableColumnStatistics(ObjectPath objectPath) throws TableNotExistException, CatalogException {
        return null;
    }

    @Override
    public CatalogTableStatistics getPartitionStatistics(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec) throws PartitionNotExistException, CatalogException {
        return null;
    }

    @Override
    public CatalogColumnStatistics getPartitionColumnStatistics(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec) throws PartitionNotExistException, CatalogException {
        return null;
    }

    @Override
    public void alterTableStatistics(ObjectPath objectPath, CatalogTableStatistics catalogTableStatistics, boolean b) throws TableNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void alterTableColumnStatistics(ObjectPath objectPath, CatalogColumnStatistics catalogColumnStatistics, boolean b) throws TableNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void alterPartitionStatistics(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec, CatalogTableStatistics catalogTableStatistics, boolean b) throws PartitionNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void alterPartitionColumnStatistics(ObjectPath objectPath, CatalogPartitionSpec catalogPartitionSpec, CatalogColumnStatistics catalogColumnStatistics, boolean b) throws PartitionNotExistException, CatalogException {
        throw new UnsupportedOperationException();
    }
}
