package org.wikimedia.eventplatform;

import org.apache.commons.compress.utils.Lists;
import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.streaming.connectors.kafka.table.KafkaDynamicTableFactory;
import org.apache.flink.table.api.ValidationException;
import org.apache.flink.table.catalog.CatalogTable;
import org.apache.flink.table.catalog.ResolvedCatalogTable;
import org.apache.flink.table.connector.sink.DynamicTableSink;
import org.apache.flink.table.factories.FactoryUtil.DefaultDynamicTableContext;
import org.apache.flink.util.StringUtils;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import static org.apache.flink.streaming.connectors.kafka.table.KafkaConnectorOptions.TOPIC;
import static org.wikimedia.eventplatform.WikimediaEventConnectorOptions.SINK_SERVER;

public class WikimediaEventDynamicTableFactory extends KafkaDynamicTableFactory {

    @Override
    public Set<ConfigOption<?>> optionalOptions() {
        Set<ConfigOption<?>> options = super.optionalOptions();
        options.add(WikimediaEventConnectorOptions.SINK_SERVER);
        return options;
    }

    @Override
    public DynamicTableSink createDynamicTableSink(Context context) {
        // Kafka connector can only sink to one topic so we modify the topic option
        Map<String, String> props = context.getCatalogTable().getOptions();

        String[] topics = props.get(TOPIC.key()).split(";");
        if(topics.length == 1) {
            return super.createDynamicTableSink(context);
        }

        String sinkPrefix = props.get(SINK_SERVER.key());
        if (StringUtils.isNullOrWhitespaceOnly(sinkPrefix)) {
            throw new ValidationException(
                String.format(
                    "Must specify s '%s' property to sink to this topic.",
                    SINK_SERVER.key()
                )
            );
        }

        String unprefixedTopic = context.getObjectIdentifier().getObjectName();
        String sinkTopic = String.format("%s.%s", sinkPrefix, unprefixedTopic);

        if (!Arrays.asList(topics).contains(sinkTopic)) {
            throw new ValidationException(
                String.format(
                    "The Kafka sink topic '%s' does not exist. Set a different '%s' property.",
                    sinkTopic, SINK_SERVER.key()
                )
            );
        }

        props.put(TOPIC.key(), sinkTopic);

        // Rebuild context from scratch with updated options because it's readonly
        ResolvedCatalogTable resolvedCatalogTable = new ResolvedCatalogTable(
            CatalogTable.of(
                context.getCatalogTable().getUnresolvedSchema(),
                null, Lists.newArrayList(),
                props
            ),
            context.getCatalogTable().getResolvedSchema()
        );

        final DefaultDynamicTableContext newContext = new DefaultDynamicTableContext(
            context.getObjectIdentifier(),
            resolvedCatalogTable,
            context.getEnrichmentOptions(),
            context.getConfiguration(),
            context.getClassLoader(),
            context.isTemporary()
        );

        return super.createDynamicTableSink(newContext);
    }
}
